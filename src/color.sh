#!/usr/bin/env bash
COLOR_RED='\033[0;31m'
COLOR_YELLOW='\033[0;93m'
COLOR_BLUE='\033[0;34m'
COLOR_GREEN='\033[0;32m'
COLOR_OFF='\033[0m'


function pprint() {
        local final_char='\n'
        local color="${END_COLOR}"
        local message=("$@")
        local indent=''
        local fd=1
        local this_function_name="$0"

        while [[ "$#" -gt 0 ]]; do
                local arg="$1"
                case "${arg}" in
                        '--help' | '-h')
                                echo "TODO: help message for pprint function"
                                return 1
                                ;;

                        '--color' | '-c')
                                # The next argument is the color choice
                                if [[ "$#" -eq 1  ]]; then
                                        echo "${this_function_name} error: color choice required after the '--color/-c' option"
                                        return 1
                                fi
                                shift
                                color="$1"
                                ;;

                        '--no-newline' | '-n')
                                final_char=''
                                ;;

                        '--indent' | '-i')
                                # The next argument is the indentation
                                if [[ "$#" -eq 1 ]]; then
                                        echo "${this_function_name} error: indent character(s) required after the '--indent/-i' option"
                                fi
                                shift
                                indent="$1"
                                ;;

                        '--stdout')
                                fd='2'
                                ;;

                        '--')
                                # Any args following '--' are considered part of the message
                                shift
                                message="$*"
                                break # Breaking here will stop us from hitting the '*' case with any other args. This way, we can print arguments that start with '-' without trying to parse as a registered option
                                ;;

                        *)
                                if [[ "${arg::1}" == '-' ]]; then
                                        echo "${this_function_name} error: unknown option '${arg}'. If your string starts with '-', then run 'pprint -- <your string>'"
                                        return 1
                                else
                                        # We can assume we've hit the first of our message to print (no more option args to parse)
                                        break
                                fi
                                ;;
                esac
                shift # Move onto next arg

        done

        eval "printf '${color}${indent}%s${END_COLOR}${final_char}' '$*' >&${fd}"
}



function print_red() {
  pprint --color "${RED}" "${@}"
}

function print_yellow() {
  pprint --color "${YELLOW}" "${@}"
}

function print_green() {
  pprint --color "${GREEN}" "${@}"
}

function print_blue() {
  pprint --color "${BLUE}" "${@}"
}

function status() {
  print_blue "> ${*}"
}

function substatus() {
  pprint --indent "${INDENT}" "${@}"
}

