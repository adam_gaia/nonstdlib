{
  description = "Nonstdlib";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        package-name = "nonstdlib";
        dev-tools = with pkgs; [
          shellcheck shfmt
        ];
        run-dependencies = with pkgs; [];
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = dev-tools;
        };

        defaultPackage = pkgs.stdenv.mkDerivation {
          name = package-name;
          src = self;
          installPhase = ''
            mkdir -p $out/src
            cp -r src/* $out/src/
            mkdir -p $out/bin
            cp bin/nonstdlib $out/bin/nonstdlib
          '';
        };
      });
}
